import React from 'react'
import c from 'classnames'

export default function Tab (props) {
  const cls = c({
    'is-active': props.active
  })
  return (
    <div id={`tab-${props.tab}`}>
      <button type='button' className={`im-button im-button_border ${cls}`}>
        {props.children}
      </button>
    </div>
  )
}
